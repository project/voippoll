VoIP Poll.module
--------------------------------------------------------------------------------
The VoIP Poll module provides phone access to Drupal polls. Using this module people can vote in polls via calls.

--------------------------
Dependencies
1.Poll (comes with Drupal core as optional module)
2.VoIP Drupal (http://drupal.org/project/voipdrupal)

--------------------------
Installation

1. Extract VoIP Poll to your sites/all/modules directory.
2. Enable the VoIP Poll module in admin/build/modules.
3. To configure the module go to admin/settings/voippoll
4. Use either voippoll_inbound or voippoll_outbound depending on your scenario
--------------------------
Settings (admin/settings/voippoll/settings)
In settings you can assign which poll to use for inbound and outbound scripts. Also you can customize some of the dialogs.


To setup your site to receive the calls and play one of the poll you should assign 
voippoll_inbound script as default VoIP Drupal script here: admin/voip/call/settings

---
The VoIP Poll module is part of the VoIP Drupal framework.  This module has been originally developed by Tamer Zoubi and Leo Burd under the 
sponsorship of the MIT Center for Future Civic Media (http://civic.mit.edu).