<?php
/**
 * @file voippoll.script.inc
 */ 

/**
 * Implementation of hook_voipcall_load_script()
 */
function voippoll_voipscript_load_script($script_name, $vars = NULL) {

  $script = NULL;
  switch ($script_name) {
    case 'voippoll_inbound': 
      $script = new VoipScript('voippoll_inbound');
      $poll_nid =  variable_get('voippoll_inbound_poll', '');
      $to_number = '%caller_number';
      voippoll_get_poll($poll_nid, $to_number, $script);
      
    break;
    case 'voippoll_outbound': 
      $script = new VoipScript('voippoll_outbound');
      $poll_nid =  variable_get('voippoll_outbound_poll', '');
      $to_number = '%dest_number';
      voippoll_get_poll($poll_nid, $to_number, $script);
    break;
  }

  return $script;
}

/*VoIP Callback function - check if this number already voted*/
function _voippoll_check_number($number, $poll_nid) {
  watchdog('voippoll', 'Checking number '.$number. ' for poll id '.$poll_nid);
  // Check to see if this caller ID has voted before. The poll module
  // hardcodes IP address, so we have to duplicate the query here
  $result = db_fetch_object(db_query("SELECT chorder FROM {poll_votes} WHERE nid = %d AND hostname = 'tel: %s'", $poll_nid, $number));
  if (isset($result->chorder)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/*VoIP Callback function - record a vote*/
function _voippoll_record_vote($number, $poll_nid, $choice) {
  watchdog('voippoll', 'Recording vote '.$choice.' for number '.$number. ' in poll id '.$poll_nid);
  db_query("INSERT INTO {poll_votes} (nid, chorder, hostname) VALUES (%d, %d, 'tel: %s')", $poll_nid, $choice, $number);
  
  // Add one to the votes.
  db_query("UPDATE {poll_choices} SET chvotes = chvotes + 1 WHERE nid = %d AND chorder = %d", $poll_nid, $choice);
  $poll = node_load($poll_nid);
  $chosen = $poll->choice[$choice]['chtext'];
  return $chosen;
}